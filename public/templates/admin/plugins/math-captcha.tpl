<div class="acp-page-container">
    <div component="settings/main/header" class="row border-bottom py-2 m-0 sticky-top acp-page-main-header align-items-center">
        <div class="col-12 col-md-8 px-0 mb-1 mb-md-0">
            <h4 class="fw-bold tracking-tight mb-0">{title}</h4>
        </div>
    </div>

    <div class="row m-0">
        <table class="table mb-4">
            <thead>
                <tr>
                    <th>[[nodebb-plugin-math-captcha:admin_counter_name]]</th>
                    <th>[[nodebb-plugin-math-captcha:admin_counter_value]]</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>[[nodebb-plugin-math-captcha:admin_failures_session]]</td>
                    <td><p class="text-end">{counters.invalid_session}</p></td>
                </tr>
                <tr>
                    <td>[[nodebb-plugin-math-captcha:admin_failures_empty]]</td>
                    <td><p class="text-end">{counters.empty}</p></td>
                </tr>
                <tr>
                    <td>[[nodebb-plugin-math-captcha:admin_failures_honeypot]]</td>
                    <td><p class="text-end">{counters.honeypot}</p></td>
                </tr>
                <tr>
                    <td>[[nodebb-plugin-math-captcha:admin_failures_nan]]</td>
                    <td><p class="text-end">{counters.not_a_number}</p></td>
                </tr>
                <tr>
                    <td>[[nodebb-plugin-math-captcha:admin_failures_wrong]]</td>
                    <td><p class="text-end">{counters.wrong}</p></td>
                </tr>
                <tr>
                    <td>[[nodebb-plugin-math-captcha:admin_total_failures_count]]</td>
                    <td><p class="text-end">{counters.total_failures}</p></td>
                </tr>
                <tr>
                    <td>[[nodebb-plugin-math-captcha:admin_correct_count]]</td>
                    <td><p class="text-end">{counters.correct}</p></td>
                </tr>
                <tr>
                    <td>[[nodebb-plugin-math-captcha:admin_created_count]]</td>
                    <td><p class="text-end">{counters.created}</p></td>
                </tr>
            </tbody>
        </table>

        <!-- IMPORT admin/partials/settings/toc.tpl -->
    </div>
</div>
